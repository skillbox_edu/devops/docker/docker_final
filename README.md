# Финальная работа курса "DevOps. Docker". Инфраструктура разработки на docker-контейнерах

Требуется развернуть инфраструктуру разработки на docker-контейнерах.

## Развертываемые сервисы
* OpenLDAP + PHP LDAP Admin
* GitLab
* Rocket Chat

## Используемые технологии
* docker
* docker compose

## Инфраструктурная схема

![infrastructure](docs/imgs/infrastructure.png)

## Подготовка сервера

В качестве сервера использовалась ВМ vagrant с установленной Ubuntu 20.04.5 LTS. Также на сервере были установлены `docker` и `docker compose`.

```shell
docker --version
```

![docker version](docs/imgs/docker-version.png)

```shell
docker compose --version
```

![docker compose version](docs/imgs/docker-compose-version.png)

## Запуск

1. По желанию можно создать файл `.env` рядом с файлом [`compose.yml`](compose.yml). Пример файла: [env.example](env.example).

1. Теперь можно запустить сервисы:

   ```shell
   docker-compose up -d
   ```
   
Адреса сервисов:
* PHP LDAP Admin - https://<you_ip>:6443
* GitLab - http://<you_ip>:8080
* Rocket Chat - http://<you_ip>:3000

## Настройка

1. Включить интеграцию с LDAP в Rocket Chat согласно [инструкции](https://docs.rocket.chat/use-rocket.chat/workspace-administration/settings/ldap)
1. Пользователи создаются через PHP LDAP Admin согласно [инструкции](docs/files/how-to-create-ldap-user.pdf)
1. Под учетками созданных пользователей можно заходить в GitLab и Rocket Chat

![gitlab login via ldap](docs/imgs/gitlab-login-via-ldap.png)

![rocketchat login via ldap](docs/imgs/rocketchat-login-via-ldap.png)

## Последовательность выполнения работы

### Задача 1. Развертывание LDAP-сервера

```shell
docker-compose up -d
docker-compose ps
```

![docker compose ps ldap](docs/imgs/docker-compose-ps-ldap.png)

### Задача 2. Добавить веб-интерфейс для созданного сервиса LDAP

```shell
docker-compose up -d
docker-compose ps
```

![docker compose ps php ldap admin](docs/imgs/docker-compose-ps-php-ldap-admin.png)

Вход в PHP LDAP Admin:
* логин - `cn=admin,dc=docker-final,dc=ru`
* пароль - из файла `.env` или `admin`, если пароль не менялся

![php ldap admin login](docs/imgs/php-ldap-admin-login.png)

### Задача 3. Добавить GitLab

```shell
docker-compose up -d
docker-compose ps
```

![docker compose ps gitlab](docs/imgs/docker-compose-ps-gitlab.png)

Вход в GitLab:
* логин - `root`
* пароль - получаем командой
  ```shell
  sudo docker-compose exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
  ```
  
![gitlab login](docs/imgs/gitlab-login.png)

### Задача 4. Интеграция GitLab и LDAP

![gitlab ldap and standard login](docs/imgs/gitlab-ldap-and-standard-login.png)

### Задача 5. Добавление пользователя в LDAP. Вход в GitLab

Созданный пользователь в PHP LDAP Admin:

![php ldap admin created user](docs/imgs/php-ldap-admin-created-user.png)

Вход в GitLab под созданным пользователем:

![gitlab login via ldap](docs/imgs/gitlab-login-via-ldap.png)

### Задача 6. Rocket Chat

```shell
docker-compose up -d
docker-compose ps
```

![docker compose ps rocketchat mongodb](docs/imgs/docker-compose-ps-rocketchat-mongodb.png)

Вход в Rocket Chat:
* для подтверждения регистрации потребуется реальный email

![rocketchat login](docs/imgs/rocketchat-login.png)

### Задача 7. Интеграция Rocket Chat и LDAP

Вход в Rocket Chat через LDAP:

![rocketchat login via ldap](docs/imgs/rocketchat-login-via-ldap.png)

## Проверка чата

В PHP LDAP Admin созданы два пользователя: `ivan` и `petr`.

Окно чата пользователя `ivan`:

![rocketchat ivan](docs/imgs/rocketchat-ivan.png)

Окно чата пользователя `petr`:

![rocketchat petr](docs/imgs/rocketchat-petr.png)
